
function loadData(){
    let request = sendRequest('operario/list', 'GET', '')
    let table = document.getElementById('users-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.id_operario}</th>
       				<td>${element.nom_op}</td>
       				<td>${element.ape_op}</td>
                    <td>${element.placa_vehiculo}</td>
                    <td>${element.incidentes}</td>
                    <td>${element.jornada}</td>
                    <td>${element.contraseña}</td>
                    
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/formulariosoperarios.html?id=${element.id_operario}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUser(${element.id_operario})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadUser(id_operario){
    let request = sendRequest('operario/list/'+id_operario, 'GET', '')
    let id = document.getElementById('user-id')
    let name = document.getElementById('user-nombre')
    let lname = document.getElementById('user-apellido')
    let nplaca = document.getElementById('user-placa') 
    let Incidentes = document.getElementById('user-incidentes')
    let dianoche = document.getElementById('user-jornada')
    let contraseña = document.getElementById('user-contraseña')
    request.onload = function(){
        
        let data = request.response;
        id.value = data.id_operario
        name.value = data.nom_op
        lname.value = data.ape_op
        nplaca.value = data.placa_vehiculo
        Incidentes.value = data.incidentes
        dianoche.value = data.jornada
        contraseña.value = data.contraseña
        
        status.checked = data.activo
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteUser(id_operario){
    let request = sendRequest('operario/'+id_operario, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}


function saveUser(){
    
    
    let name = document.getElementById('user-nombre').value
    let lname = document.getElementById('user-apellido').value
    let nplaca = document.getElementById('user-placa').value
    let Incidentes = document.getElementById('user-incidentes').value
    let dianoche = document.getElementById('user-jornada').value
    let contraseña = document.getElementById('user-contraseña').value
    let id = document.getElementById('user-id').value
    
//'id_operario' : id,
    let data = {'id_operario' : id,'nom_op' : name, 'ape_op' : lname, 'placa_vehiculo' : nplaca, 'incidentes':Incidentes , 'jornada' : dianoche, 'contraseña':contraseña}
    let request = sendRequest('operario/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'operarios.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}
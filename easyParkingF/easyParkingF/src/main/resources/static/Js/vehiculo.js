
function loadData(){
    let request = sendRequest('vehiculo/list', 'GET', '')
    let table = document.getElementById('vehiculo-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.id_vehiculo}</th>
                    <td>${element.placa}</td>
                    <td>${element.duration_minutos}</td>
                    <td>${element.fecha_salida}</td>
                    <td>${element.fecha_entrada}</td>
                    <td>${element.tipo_vehiculo}</td>
                    <td>${element.tipo_membresia}</td>
                    <td>${element.num_plaza}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/formulariovehiculos.html?id=${element.id_vehiculo}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='delete_vehiculo(${element.id_vehiculo})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function load_vehiculo(id_vehiculo){
    let request = sendRequest('vehiculo/list/'+id_vehiculo, 'GET', '')
    let id = document.getElementById('vehiculo-id')
    let placa=document.getElementById("vehiculo-placa")
    let duration_minutos = document.getElementById('vehiculo-duration_minutos')
    let type_vehiculo = document.getElementById('vehiculo-tipo_vehiculo')
    let type_membresia = document.getElementById('vehiculo-tipo_membresia')
    let number_plaza = document.getElementById('vehiculo-tipo_num_plaza')
    let date_entrada = document.getElementById('vehiculo-fecha_entrada')
    let date_salida = document.getElementById('vehiculo-fecha_salida')
  

    
    request.onload = function(){
        
        let data = request.response
        id.value = data.id_vehiculo
        placa.value=data.placa
        duration_minutos.value = data.duration_minutos
        type_vehiculo.value = data.tipo_vehiculo
	    type_membresia.value = data.tipo_membresia
	    number_plaza.value = data.num_plaza
	    date_entrada.value = data.fecha_entrada
	    date_salida.value = data.fecha_salida
	    
	   
        
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function delete_vehiculo(id_vehiculo){
    let request = sendRequest('vehiculo/'+id_vehiculo, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveUser(){
    let id = document.getElementById('vehiculo-id').value
    let placa=document.getElementById("vehiculo-placa").value
    let duration_minutos = document.getElementById('vehiculo-duration_minutos').value
    let date_salida = document.getElementById('vehiculo-fecha_salida').value
    let date_entrada = document.getElementById('vehiculo-fecha_entrada').value
    let type_vehiculo = document.getElementById('vehiculo-tipo_vehiculo').value
    let type_membresia = document.getElementById('vehiculo-tipo_membresia').value
    let number_plaza = document.getElementById('vehiculo-tipo_num_plaza').value

    let data = {"id_vehiculo":id,'placa': placa,'duration_minutos':duration_minutos,'fecha_salida': date_salida, 'fecha_entrada': date_entrada, 'tipo_vehiculo':type_vehiculo, 'tipo_membresia':type_membresia,'num_plaza':number_plaza}
    let request = sendRequest('vehiculo/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'vehiculo.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}
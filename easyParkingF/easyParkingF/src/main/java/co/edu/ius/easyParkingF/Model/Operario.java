/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.ius.easyParkingF.Model;



import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author jupac
 */


@Entity
@Table(name = "Operario")
public class Operario implements Serializable{

   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_operario")
    private Integer id_operario;
    @Column(name = "nom_op")
    private String nom_op;
    @Column(name = "ape_op")
    private String ape_op;
    @Column(name = "placa_vehiculo")
    private String placa_vehiculo;
    @Column(name = "incidentes")
    private String incidentes;
    @Column(name = "jornada")
    private String jornada;
	@Column(name = "contraseña")
	private String contraseña;
	public Integer getId_operario() {
		return id_operario;
	}
	public void setId_operario(Integer id_operario) {
		this.id_operario = id_operario;
	}
	public String getNom_op() {
		return nom_op;
	}
	public void setNom_op(String nom_op) {
		this.nom_op = nom_op;
	}
	public String getApe_op() {
		return ape_op;
	}
	public void setApe_op(String ape_op) {
		this.ape_op = ape_op;
	}
	public String getPlaca_vehiculo() {
		return placa_vehiculo;
	}
	public void setPlaca_vehiculo(String placa_vehiculo) {
		this.placa_vehiculo = placa_vehiculo;
	}
	public String getIncidentes() {
		return incidentes;
	}
	public void setIncidentes(String incidentes) {
		this.incidentes = incidentes;
	}
	public String getJornada() {
		return jornada;
	}
	public void setJornada(String jornada) {
		this.jornada = jornada;
	}
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	

   
    
    
}



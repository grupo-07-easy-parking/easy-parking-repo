
package co.edu.ius.easyParkingF.Controller;

import co.edu.ius.easyParkingF.Model.Parqueadero;
import co.edu.ius.easyParkingF.Services.ParqueaderoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Jona curso
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/parqueadero")

public class ParqueaderoController {
  
    
    @Autowired
private ParqueaderoService parqueaderoservice;
 
  @PostMapping(value="/")
public ResponseEntity<Parqueadero> agregar(@RequestBody Parqueadero parqueadero){
Parqueadero obj = parqueaderoservice.save(parqueadero);
return new ResponseEntity<>(obj, HttpStatus.OK);
}   
    
 @DeleteMapping(value="/{id}")
public ResponseEntity<Parqueadero> eliminar(@PathVariable Integer id){
Parqueadero obj = parqueaderoservice.findById(id);
if(obj!=null)
 parqueaderoservice.delete(id);
else
 return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}   


 @PutMapping(value="/")
public ResponseEntity<Parqueadero> editar(@RequestBody Parqueadero parqueadero){
Parqueadero obj = parqueaderoservice.findById(parqueadero.getId_plaza());
if(obj!=null)
{
 obj.setEsta_disponible(parqueadero.getEsta_disponible());
 obj.setNumero_disponibles(parqueadero.getNumero_disponibles());

parqueaderoservice.save(obj);
}
else
 return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}
    
 @GetMapping("/list")
public List<Parqueadero> consultarTodo(){
return parqueaderoservice.findAll();
}
@GetMapping("/list/{id}")
public Parqueadero consultaPorId(@PathVariable Integer id){
return parqueaderoservice.findById(id);
}

    
    
    
}



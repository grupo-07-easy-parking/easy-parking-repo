package co.edu.ius.easyParkingF.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ui.Model;

import co.edu.ius.easyParkingF.Model.Vehiculo;

import co.edu.ius.easyParkingF.Services.VehiculoService;



@RestController
@CrossOrigin("*")
@RequestMapping("/vehiculo")

public class VehiculoController {

	
    @Autowired
private VehiculoService vehiculoservice;
 
  @PostMapping(value="/")
public ResponseEntity<Vehiculo> agregar(@RequestBody Vehiculo vehiculo){
Vehiculo obj = vehiculoservice.save(vehiculo);
return new ResponseEntity<>(obj, HttpStatus.OK);
}   
    
 @DeleteMapping(value="/{id}")
public ResponseEntity<Vehiculo> eliminar(@PathVariable Integer id){
Vehiculo obj = vehiculoservice.findById(id);
if(obj!=null)
 vehiculoservice.delete(id);
else
 return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}   


 @PutMapping(value="/")
public ResponseEntity<Vehiculo> editar(@RequestBody Vehiculo vehiculo){
Vehiculo obj = vehiculoservice.findById(vehiculo.getId_vehiculo());
if(obj!=null)
{
 obj.setDuration_minutos(vehiculo.getDuration_minutos());
 obj.setFecha_salida(vehiculo.getFecha_salida());
 obj.setFecha_entrada(vehiculo.getFecha_entrada());
 obj.setNum_plaza(vehiculo.getNum_plaza());
 obj.setTipo_membresia(vehiculo.getTipo_membresia());
 obj.setTipo_vehiculo(vehiculo.getTipo_vehiculo());
 obj.setPlaca(vehiculo.getPlaca());

vehiculoservice.save(obj);
}
else
 return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}
    
 @GetMapping("/list")
public List<Vehiculo> consultarTodo(){
return vehiculoservice.findAll();
}
@GetMapping("/list/{id}")
public Vehiculo consultaPorId(@PathVariable Integer id){
return vehiculoservice.findById(id);
}

}

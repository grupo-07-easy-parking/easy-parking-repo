/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.ius.easyParkingF.Dao;
import co.edu.ius.easyParkingF.Model.Operario;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Emmanuel
 */
public interface OperarioDao extends
CrudRepository<Operario,Integer> {

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.ius.easyParkingF.Services;
import co.edu.ius.easyParkingF.Model.Vehiculo;
import java.util.List;
/**
 *
 * @author jupac
 */
public interface VehiculoService {
 public Vehiculo save(Vehiculo vehiculo);
 public void delete(Integer id);
 public Vehiculo findById(Integer id);
 public List<Vehiculo> findAll();   
}


package co.edu.ius.easyParkingF.Model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author JL
 */
@Entity
@Table(name = "factura_parqueo")

public class Factura_parqueo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura")
    private Integer id_factura;

    @Column(name = "id_operario")
    private Integer id_operario;

    @Column(name = "id_plaza_parqueo")
    private String id_plaza_parqueo;
    
    @Column(name = "tiempo_entrada")
    private String tiempo_entrada;
    
    @Column(name = "valor_servicios")
    private Integer valor_servicios;

    public Integer getId_factura() {
        return id_factura;
    }

    public void setId_factura(Integer id_factura) {
        this.id_factura = id_factura;
    }

    public Integer getId_operario() {
        return id_operario;
    }

    public void setId_operario(Integer id_operario) {
        this.id_operario = id_operario;
    }

    public String getId_plaza_parqueo() {
        return id_plaza_parqueo;
    }

    public void setId_plaza_parqueo(String id_plaza_parqueo) {
        this.id_plaza_parqueo = id_plaza_parqueo;
    }

    public String getTiempo_entrada() {
        return tiempo_entrada;
    }

    public void setTiempo_entrada(String tiempo_entrada) {
        this.tiempo_entrada = tiempo_entrada;
    }

    public Integer getValor_servicios() {
        return valor_servicios;
    }

    public void setValor_servicios(Integer valor_servicios) {
        this.valor_servicios = valor_servicios;
    }


}


package co.edu.ius.easyParkingF.Dao;

import co.edu.ius.easyParkingF.Model.Factura_parqueo;
import org.springframework.data.repository.CrudRepository;


public interface Factura_parqueoDao  extends
CrudRepository<Factura_parqueo,Integer> {
    
}

package co.edu.ius.easyParkingF.Services.Implement;

import co.edu.ius.easyParkingF.Dao.ParqueaderoDao;
import co.edu.ius.easyParkingF.Model.Parqueadero;
import co.edu.ius.easyParkingF.Services.ParqueaderoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service

public class ParqueaderoServiceImpl implements ParqueaderoService {

    @Autowired
    private ParqueaderoDao parqueaderoDao;

    @Override
    @Transactional(readOnly = false)
    public Parqueadero save(Parqueadero parqueadero) {
        return parqueaderoDao.save(parqueadero);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        parqueaderoDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Parqueadero findById(Integer id) {
        return parqueaderoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Parqueadero> findAll() {
        return (List<Parqueadero>) parqueaderoDao.findAll();
    }

}

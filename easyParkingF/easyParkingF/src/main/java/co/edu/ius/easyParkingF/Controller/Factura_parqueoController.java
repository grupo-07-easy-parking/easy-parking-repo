
package co.edu.ius.easyParkingF.Controller;

import co.edu.ius.easyParkingF.Model.Factura_parqueo;
import co.edu.ius.easyParkingF.Services.Factura_parqueoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
@RequestMapping("/factura_parqueo")

public class Factura_parqueoController {
  
    
@Autowired
private Factura_parqueoService factura_parqueoservice;
 
@PostMapping(value="/")
public ResponseEntity<Factura_parqueo> agregar(@RequestBody Factura_parqueo factura_parqueo){
Factura_parqueo obj = factura_parqueoservice.save(factura_parqueo);
return new ResponseEntity<>(obj, HttpStatus.OK);
}   
    
@DeleteMapping(value="/{id}")
public ResponseEntity<Factura_parqueo> eliminar(@PathVariable Integer id){
Factura_parqueo obj = factura_parqueoservice.findById(id);
if(obj!=null)
 factura_parqueoservice.delete(id);
else
return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}   


@PutMapping(value="/")
public ResponseEntity<Factura_parqueo> editar(@RequestBody Factura_parqueo factura_parqueo){
Factura_parqueo obj = factura_parqueoservice.findById(factura_parqueo.getId_factura());
if(obj!=null)
{
 obj.setId_operario(factura_parqueo.getId_operario());
 obj.setId_plaza_parqueo(factura_parqueo.getId_plaza_parqueo());
 obj.setTiempo_entrada(factura_parqueo.getTiempo_entrada());
 obj.setValor_servicios(factura_parqueo.getValor_servicios());

factura_parqueoservice.save(obj);
}
else
return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}
    
 @GetMapping("/list")
public List<Factura_parqueo> consultarTodo(){
return factura_parqueoservice.findAll();
}
@GetMapping("/list/{id}")
public Factura_parqueo consultaPorId(@PathVariable Integer id){
return factura_parqueoservice.findById(id);
}
        
}



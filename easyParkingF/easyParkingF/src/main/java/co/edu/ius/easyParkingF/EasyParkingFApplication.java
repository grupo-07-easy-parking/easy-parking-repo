package co.edu.ius.easyParkingF;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyParkingFApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyParkingFApplication.class, args);
	}

}

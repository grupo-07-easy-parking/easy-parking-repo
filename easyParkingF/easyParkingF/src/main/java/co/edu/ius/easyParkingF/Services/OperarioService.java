
package co.edu.ius.easyParkingF.Services;

import co.edu.ius.easyParkingF.Model.Operario;
import java.util.List;


public interface OperarioService {

 public Operario save(Operario operario);
 public void delete(Integer id);
 public Operario findById(Integer id);
 public List<Operario> findAll();



}

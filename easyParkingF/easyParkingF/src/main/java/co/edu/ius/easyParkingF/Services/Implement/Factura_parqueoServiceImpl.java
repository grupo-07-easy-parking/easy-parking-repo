
package co.edu.ius.easyParkingF.Services.Implement;

import co.edu.ius.easyParkingF.Dao.Factura_parqueoDao;
import co.edu.ius.easyParkingF.Model.Factura_parqueo;
import co.edu.ius.easyParkingF.Services.Factura_parqueoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Factura_parqueoServiceImpl implements Factura_parqueoService{
    
    @Autowired
    private Factura_parqueoDao factura_parqueoDao;
    
    @Override
    @Transactional(readOnly = false)
    public Factura_parqueo save (Factura_parqueo factura_parqueo){
        return factura_parqueoDao.save(factura_parqueo);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        factura_parqueoDao.deleteById(id);}

    @Override
    @Transactional(readOnly = true)
    public Factura_parqueo findById(Integer id) {
        return factura_parqueoDao.findById(id).orElse(null); }

    @Override
    @Transactional(readOnly = true)
    public List<Factura_parqueo> findAll() {
        return (List<Factura_parqueo>) factura_parqueoDao.findAll();
    }
    
}

package co.edu.ius.easyParkingF.Model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "parqueadero")

public class Parqueadero implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_plaza")
    private Integer id_plaza;

    @Column(name = "esta_disponible")
    private Integer esta_disponible;

    @Column(name = "numero_disponibles")
    private Integer numero_disponibles;

    public Integer getId_plaza() {
        return id_plaza;
    }

    public void setId_plaza(Integer id_plaza) {
        this.id_plaza = id_plaza;
    }

    public Integer getEsta_disponible() {
        return esta_disponible;
    }

    public void setEsta_disponible(Integer esta_disponible) {
        this.esta_disponible = esta_disponible;
    }

    public Integer getNumero_disponibles() {
        return numero_disponibles;
    }

    public void setNumero_disponibles(Integer numero_disponibles) {
        this.numero_disponibles = numero_disponibles;
    }

}

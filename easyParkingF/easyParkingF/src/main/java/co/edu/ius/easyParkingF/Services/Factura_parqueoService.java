
package co.edu.ius.easyParkingF.Services;

import co.edu.ius.easyParkingF.Model.Factura_parqueo;
import java.util.List;


public interface Factura_parqueoService {
    
 public Factura_parqueo save(Factura_parqueo factura);
 public void delete(Integer id);
 public Factura_parqueo findById(Integer id);
 public List<Factura_parqueo> findAll();   
       
     
}

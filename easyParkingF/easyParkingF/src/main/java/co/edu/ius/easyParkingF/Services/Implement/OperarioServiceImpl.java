package co.edu.ius.easyParkingF.Services.Implement;

import co.edu.ius.easyParkingF.Dao.OperarioDao;
import co.edu.ius.easyParkingF.Model.Operario;
import co.edu.ius.easyParkingF.Services.OperarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service

public class OperarioServiceImpl implements OperarioService {

    @Autowired
    private OperarioDao operarioDao;

   @Override
    @Transactional(readOnly = false)
    public Operario save(Operario operario) {
          return operarioDao.save(operario); 
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        operarioDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Operario findById(Integer id) {
        return operarioDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Operario> findAll() {
        return (List<Operario>) operarioDao.findAll();
    }



}

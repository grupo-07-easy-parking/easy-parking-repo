package co.edu.ius.easyParkingF.Controller;

import co.edu.ius.easyParkingF.Model.Operario;
import co.edu.ius.easyParkingF.Services.OperarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Emmanuel
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/operario")

public class OperarioController {


    @Autowired
private OperarioService operarioservice;

  @PostMapping(value="/")
public ResponseEntity<Operario> agregar(@RequestBody Operario operario){
Operario obj = operarioservice.save(operario);
return new ResponseEntity<>(obj, HttpStatus.OK);
}

 @DeleteMapping(value="/{id}")
public ResponseEntity<Operario> eliminar(@PathVariable Integer id){
Operario obj = operarioservice.findById(id);
if(obj!=null)
 operarioservice.delete(id);
else
 return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}


 @PutMapping(value="/")
public ResponseEntity<Operario> editar(@RequestBody Operario operario){
Operario obj = operarioservice.findById(operario.getId_operario());
if(obj!=null)
{
    
 obj.setNom_op(operario.getNom_op());
 obj.setApe_op(operario.getApe_op());
 obj.setPlaca_vehiculo(operario.getPlaca_vehiculo());
 obj.setIncidentes(operario.getIncidentes());
 obj.setJornada(operario.getJornada());
 obj.setContraseña(operario.getContraseña());


operarioservice.save(obj);
}
else
 return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}


 @GetMapping("/list")
public List<Operario> consultarTodo(){
return operarioservice.findAll();
}
@GetMapping("/list/{id}")
public Operario consultaPorId(@PathVariable Integer id){
return operarioservice.findById(id);
}


}

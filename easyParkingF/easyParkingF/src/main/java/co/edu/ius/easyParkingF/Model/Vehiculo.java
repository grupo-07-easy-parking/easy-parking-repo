/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.ius.easyParkingF.Model;



import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author jupac
 */


@Entity
@Table(name = "vehiculo")
public class Vehiculo implements Serializable{
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_vehiculo")
    private Integer id_vehiculo;
    
    @Column(name = "placa")
    private String placa;
 
    @Column(name = "fecha_salida")
    private String fecha_salida;
    @Column(name = "fecha_entrada")
    private String fecha_entrada;
    @Column(name = "duration_minutos")
    private Integer duration_minutos;
    @Column(name = "tipo_vehiculo")
    private String tipo_vehiculo;
    @Column(name = "tipo_membresia")
    private String tipo_membresia;
    @Column(name = "num_plaza")
    private String num_plaza;
    
    
    
    
	public Integer getId_vehiculo() {
		return id_vehiculo;
	}
	public void setId_vehiculo(Integer id_vehiculo) {
		this.id_vehiculo = id_vehiculo;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getFecha_salida() {
		return fecha_salida;
	}
	public void setFecha_salida(String fecha_salida) {
		this.fecha_salida = fecha_salida;
	}
	public String getFecha_entrada() {
		return fecha_entrada;
	}
	public void setFecha_entrada(String fecha_entrada) {
		this.fecha_entrada = fecha_entrada;
	}
	public Integer getDuration_minutos() {
		return duration_minutos;
	}
	public void setDuration_minutos(Integer duration_minutos) {
		this.duration_minutos = duration_minutos;
	}
	public String getTipo_vehiculo() {
		return tipo_vehiculo;
	}
	public void setTipo_vehiculo(String tipo_vehiculo) {
		this.tipo_vehiculo = tipo_vehiculo;
	}
	public String getTipo_membresia() {
		return tipo_membresia;
	}
	public void setTipo_membresia(String tipo_membresia) {
		this.tipo_membresia = tipo_membresia;
	}
	public String getNum_plaza() {
		return num_plaza;
	}
	public void setNum_plaza(String num_plaza) {
		this.num_plaza = num_plaza;
	}
	
    
    
    
    
            
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.ius.easyParkingF.Dao;
import co.edu.ius.easyParkingF.Model.Vehiculo;


import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author jupac
 */
public interface VehiculoDao extends
CrudRepository<Vehiculo,Integer> {
    
}


package co.edu.ius.easyParkingF.Dao;

import co.edu.ius.easyParkingF.Model.Parqueadero;
import org.springframework.data.repository.CrudRepository;


public interface ParqueaderoDao  extends
CrudRepository<Parqueadero,Integer> {
    
}

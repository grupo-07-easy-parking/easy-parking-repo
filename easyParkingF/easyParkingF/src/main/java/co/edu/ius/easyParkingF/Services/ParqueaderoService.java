
package co.edu.ius.easyParkingF.Services;

import co.edu.ius.easyParkingF.Model.Parqueadero;
import java.util.List;


public interface ParqueaderoService {
    
 public Parqueadero save(Parqueadero parqueadero);
 public void delete(Integer id);
 public Parqueadero findById(Integer id);
 public List<Parqueadero> findAll();   
       
    
    
}

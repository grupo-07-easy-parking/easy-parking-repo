-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema easyparking
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema easyparking
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `easyparking` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `easyparking` ;

-- -----------------------------------------------------
-- Table `easyparking`.`operario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `easyparking`.`operario` (
  `id_operario` INT NOT NULL AUTO_INCREMENT,
  `nom_op` VARCHAR(45) NOT NULL,
  `ape_op` VARCHAR(45) NOT NULL,
  `placa_vehiculo` VARCHAR(30) NOT NULL,
  `Incidentes` VARCHAR(100) NULL DEFAULT NULL,
  `Jornada` VARCHAR(45) NOT NULL,
  `contraseņa` VARCHAR(40) NULL DEFAULT NULL,
  PRIMARY KEY (`id_operario`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `easyparking`.`factura_parqueo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `easyparking`.`factura_parqueo` (
  `id_factura` INT NOT NULL AUTO_INCREMENT,
  `id_operario` INT NOT NULL,
  `id_plaza_parqueo` VARCHAR(30) NOT NULL,
  `tiempo_entrada` TIMESTAMP NOT NULL,
  `valor_servicios` BIGINT NOT NULL,
  PRIMARY KEY (`id_factura`),
  UNIQUE INDEX `id_factura_UNIQUE` (`id_factura` ASC) VISIBLE,
  INDEX `factura_cliente_fk` (`id_operario` ASC) VISIBLE,
  CONSTRAINT `factura_cliente_fk`
    FOREIGN KEY (`id_operario`)
    REFERENCES `easyparking`.`operario` (`id_operario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `easyparking`.`parqueadero`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `easyparking`.`parqueadero` (
  `id_plaza` VARCHAR(30) NOT NULL,
  `esta_disponible` TINYINT NOT NULL,
  `numero_disponibles` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_plaza`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `easyparking`.`vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `easyparking`.`vehiculo` (
  `id_vehiculo` INT NOT NULL AUTO_INCREMENT,
  `placa` VARCHAR(30) NOT NULL DEFAULT 'Placa',
  `duration_minutos` INT NULL DEFAULT NULL,
  `fecha_salida` DATETIME NULL DEFAULT NULL,
  `fecha_entrada` DATETIME NULL DEFAULT NULL,
  `tipo_vehiculo` VARCHAR(30) NOT NULL,
  `tipo_membresia` VARCHAR(45) NULL DEFAULT NULL,
  `num_plaza` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_vehiculo`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

const { exit } = require("process");

//METODO PARA LISTAR
function listarUsuarios()
{
    let request = sendRequest('parqueadero/list', 'GET', '');
    let tabla = document.getElementById('tablajparqueadero');
    tabla.innerHTML = "";
    request.onload = function()
    {
        let datos = request.response;
        datos.forEach(element =>
        {
            tabla.innerHTML += `
            <tr>
                <th scope="row">${element.id}</th>
                <td>${element.nombre}</td>
                <td>${element.correo}</td>
                <td><button type="button" class="btn btn-primary" onclick='window.location = "usuariosdetalle.html?id=${element.id}"'>Editar</button></td>
                <td><button type="button" class="btn btn-danger" onclick='borrarUsuario(${element.id})'>Borrar</button></td>}
            </tr>
            `
        });
    }
    request.onerror = function()
    {
        tabla.innerHTML = `
        <tr>
            <td colspan='6'>Error al cargar los datos</td>
        </tr>
        `;
    }
}
//METODO PARA BORRAR
function borrarUsuario(id)
{
    let request = sendRequest('usuario/'+id, 'DELETE', '');
    request.onload = function()
    {
        listarUsuarios();
    }
}

//METODO PARA CARGAR
function cargarUsuario(id)
{
    let request = sendRequest('usuario/list/'+id, 'GET', '');
    let idusuario = document.getElementById('id');
    let usuario = document.getElementById('usuario');
    let cedula = document.getElementById('cedula');
    let nombre = document.getElementById('nombre');
    let celular = document.getElementById('celular');
    let correo = document.getElementById('correo');
    let activo = document.getElementById('activo');
    let clave = document.getElementById('clave');
    request.onload = function()
    {
        let datos = request.response;
        idusuario.value = datos.id;
        usuario.value = datos.usuario;
        cedula.value = datos.cedula;
        nombre.value = datos.nombre;
        celular.value = datos.celular;
        correo.value = datos.correo;
        activo.checked = datos.activo;
        clave.value = datos.clave;

    }
    request.onerror = function()
    {
        alert("Error al cargar el registro");
    }
}

//METODO PARA AGREGAR O ACTUALIZAR
function grabarUsuario()
{
    let idusuario = document.getElementById('id').value
    let usuario = document.getElementById('usuario').value
    let cedula = document.getElementById('cedula').value
    let nombre = document.getElementById('nombre').value
    let celular = document.getElementById('celular').value
    let correo = document.getElementById('correo').value
    let activo = document.getElementById('activo').checked
    let clave = document.getElementById('clave').value
    var datos = {'id':idusuario,'usuario':usuario,'cedula':cedula,'nombre':nombre,'celular':celular,'correo':correo,'activo':activo,'clave':clave}
    let request = sendRequest('usuario/', idusuario ? 'PUT':'POST', datos)
    request.onload = function()
    {
        window.location = "usuarios.html";
    }
    request.onerror = function()
    {
        alert("Error al guardar el registro");
    }
}